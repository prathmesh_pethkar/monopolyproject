
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;
import java.util.Random;
import javax.swing.JButton;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author prathmesh
 */
public class Jail extends javax.swing.JFrame {

    int turn;
    DB d;
    PreparedStatement psql;
    ResultSet players;
    Board currentBoard;
    public Jail(Board b,int turn) {
        initComponents();
        try {
            if(getBalance(turn+1)<500)
            {
                jButton1.setEnabled(false);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Jail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Jail.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.turn = turn;
        currentBoard = b;
        try {
            d = new DB();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Jail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Jail.class.getName()).log(Level.SEVERE, null, ex);
        }
        currentBoard.setEnabled(false);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        jButton1.setBackground(new java.awt.Color(255, 0, 0));
        jButton1.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jButton1.setText("PAY 500");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(0, 0, 255));
        jButton2.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jButton2.setText("ROLL FOR DOUBLE");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        currentBoard.setEnabled(true);
        currentBoard.done.setEnabled(false);
        currentBoard.done.setVisible(false);
        currentBoard.dice.setEnabled(true);
        try {
            if(getBalance(turn+1)>500)
            {
                currentBoard.Transaction(turn+1, -500);
                this.dispose();
            }
            else
            {
                BalanceMsg bmsg = new BalanceMsg(this);
                bmsg.setVisible(true);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Jail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Jail.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        currentBoard.setEnabled(true);
        currentBoard.dice.setEnabled(false);
        JButton dice = new JButton();
        dice.setIcon(currentBoard.dice.getIcon()); // NOI18N
        dice.setPreferredSize(new java.awt.Dimension(100, 50));
        dice.setVisible(true);
        currentBoard.jPanel3.add(dice);
        dice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                currentBoard.done.setVisible(true);
                currentBoard.done.setEnabled(true);
                dice.setEnabled(false);
                
                Random ran;
                ran = new Random();
                int d1=0,d2=0;
                while(d1==0 || d2==0)
                {
                    d1 = ran.nextInt(7);
                    d2 = ran.nextInt(7);
                }
                String dice1 = Integer.toString(d1);
                String dice2 = Integer.toHexString(d2);
                dice.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/dice"+dice1+dice2+".png"));
                currentBoard.dice.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/dice"+dice1+dice2+".png"));
                System.out.println(".actionPerformed()" + d1 + " " + d2);
                if(d1==d2)
                {
                    try
                    {
                        switch(turn+1)
                        {
                            case 1: currentBoard.move(currentBoard.bluePawn, d1+d2,9,turn);
                                    break;

                            case 2: currentBoard.move(currentBoard.redPawn, d1+d2,9,turn);
                                    break;

                            case 3: currentBoard.move(currentBoard.greenPawn, d1+d2,9,turn);
                                    break;

                            case 4: currentBoard.move(currentBoard.yellowPawn, d1+d2,9,turn);
                                    break;

                        }
                    }
                    catch (SQLException ex) {
                        Logger.getLogger(BuyAuction.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Jail.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                currentBoard.jPanel3.remove(dice);
            }
        });
        //currentBoard.jPanel2.add(dice);
        dice.setBounds(currentBoard.dice.getBounds());
        
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    public int getBalance(int turn) throws ClassNotFoundException, SQLException
    {
        DB db = new DB();
        int balance = 0;
        PreparedStatement psql = db.players.prepareStatement("SELECT * FROM players WHERE number=?");
        psql.setInt(1, turn);
        ResultSet players = psql.executeQuery();
        while(players.next())
        {
            balance = players.getInt("balance");
        }
        return balance;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    // End of variables declaration//GEN-END:variables
}
