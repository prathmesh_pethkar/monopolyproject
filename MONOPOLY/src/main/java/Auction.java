
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;

public class Auction extends javax.swing.JFrame {
    Board currentBoard;
    DB d;
    PreparedStatement psql;
    int no_players;
    int turn;
    int lastBid;
    int lastBidder;
    int position;
    int balance;
    String city;
    List<Integer> folded;
    Auction a;
    public Auction(Board currentBoard,int turn,int no_players,int position) {
        this.currentBoard = currentBoard;
        a = this;
        //currentBoard.setEnabled(false);
        this.turn = turn;
        this.no_players = no_players;
        this.position = position;
        folded  = new ArrayList<Integer>();
        lastBid = 0;
        lastBidder = 0;
        try {
            d = new DB();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
        }
        balance = getBalance(turn+1);
        city = getCity(position);
        city = city.toUpperCase();
        initComponents();
        while(balance<(lastBid+1))
        {
            lastRecord.setText("Player " + (this.turn+1) + " folded");
            lastRecord.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            lastRecord.setBackground(currentBoard.getColor(turn+1));
            folded.add(this.turn+1);
            check();
            System.out.println("till here");
//            if(!this.isActive())
//            {
//                return;
//            }
            System.out.println("dis not end");
            do
            {
                System.out.println("in do while  " + (this.turn+1));
                this.turn = (this.turn+1)%no_players;
            }while(folded.contains(this.turn+1));
            balance = getBalance(this.turn+1);
            System.out.println("In while  " + (this.turn+1));
        }
        
        currentBidder.setText("Player " + (this.turn+1) + " is bidding...");
        slider.setMaximum(balance);
        slider.setMinimum(lastBid+1);
        slider.setEnabled(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        card = new javax.swing.JLabel();
        slider = new javax.swing.JSlider();
        currentBidder = new javax.swing.JLabel();
        bid = new javax.swing.JButton();
        fold = new javax.swing.JButton();
        lastRecord = new javax.swing.JLabel();
        currentBid = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        card.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        card.setMaximumSize(new java.awt.Dimension(175, 185));
        card.setMinimumSize(new java.awt.Dimension(175, 185));
        card.setPreferredSize(new java.awt.Dimension(175, 185));
        card.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/Card_"+city+".jpg"));

        slider.setMinorTickSpacing(100);
        slider.setPaintLabels(true);
        slider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderStateChanged(evt);
            }
        });

        currentBidder.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N

        bid.setBackground(new java.awt.Color(0, 255, 0));
        bid.setText("BID");
        bid.setMaximumSize(new java.awt.Dimension(60, 30));
        bid.setMinimumSize(new java.awt.Dimension(60, 30));
        bid.setPreferredSize(new java.awt.Dimension(60, 30));
        bid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bidActionPerformed(evt);
            }
        });

        fold.setBackground(new java.awt.Color(255, 0, 0));
        fold.setText("FOLD");
        fold.setMaximumSize(new java.awt.Dimension(60, 30));
        fold.setMinimumSize(new java.awt.Dimension(60, 30));
        fold.setPreferredSize(new java.awt.Dimension(60, 30));
        fold.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                foldActionPerformed(evt);
            }
        });

        lastRecord.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        lastRecord.setOpaque(true);

        currentBid.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(bid, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(card, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(slider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(currentBidder, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lastRecord, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(fold, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(currentBid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(currentBidder, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lastRecord, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(currentBid, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(slider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(card, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bid, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fold, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bidActionPerformed
        // TODO add your handling code here:
        //fold.setEnabled(true);
        Thread t = new Thread(){
            @Override
            public void run()
            {
                lastBid = slider.getValue();
                lastBidder = turn+1;
                lastRecord.setText("Player " + (turn+1) + " bid Rs." + lastBid);
                lastRecord.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                lastRecord.setBackground(currentBoard.getColor(turn+1));
                check();
                if(folded.size()==(no_players-1))
                {
                    return;
                }
                do
                {
                    turn = (turn+1)%no_players;
                }while(folded.contains(turn+1));
                balance = getBalance(turn+1);
                try {
                    sleep((long)300);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
                }
                while(balance<(lastBid+1) && (turn+1)!=lastBidder)
                {
                    lastRecord.setText("Player " + (turn+1) + " folded");
                    lastRecord.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                    lastRecord.setBackground(currentBoard.getColor(turn+1));
                    folded.add(turn+1);
                    try {
                        sleep((long)300);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    check();
                    if((turn+1)==lastBidder)
                    {
                        return;
                    }
                    do
                    {
                        turn = (turn+1)%no_players;
                    }while(folded.contains(turn+1));
                    balance = getBalance(turn+1);
                }


                slider.setMinimum(lastBid+1);
                slider.setMaximum(balance);
                currentBidder.setText("Player " + (turn+1) + " is bidding...");
            }
        };
        t.start();
    }//GEN-LAST:event_bidActionPerformed

    private void foldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_foldActionPerformed
        // TODO add your handling code here:
        Thread t1 = new Thread(){
            @Override
            public void run()
            {
                folded.add(turn+1);
                lastRecord.setText("Player " + (turn+1) + " folded");
                lastRecord.setBackground(currentBoard.getColor(turn+1));
                lastRecord.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                try {
                    sleep((long)300);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
                }
                check();
                if(folded.size()==no_players)
                {
                    return;
                }
                System.out.println("still here....");
                do
                {
                    turn = (turn+1)%no_players;
                }while(folded.contains(turn+1));
                balance = getBalance(turn+1);
                try {
                    sleep((long)300);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
                }
                while(balance<(lastBid+1) && (turn+1)!=lastBidder)
                {
                    lastRecord.setText("Player " + (turn+1) + " folded");
                    lastRecord.setBackground(currentBoard.getColor(turn+1));
                    lastRecord.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                    folded.add(turn+1);
                    try {
                        sleep((long)300);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    check();
                    if((turn+1)==lastBidder)
                    {
                        return;
                    }
                    do
                    {
                        turn = (turn+1)%no_players;
                    }while(folded.contains(turn+1));
                    balance = getBalance(turn+1);
                }
                slider.setMinimum(lastBid+1);
                slider.setMaximum(balance);
                currentBidder.setText("Player " + (turn+1) + " is bidding...");
            }
        };
        t1.start();
    }//GEN-LAST:event_foldActionPerformed

    private void sliderStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderStateChanged
        // TODO add your handling code here:
        currentBid.setText(Integer.toString(slider.getValue()));
        currentBid.setForeground(currentBoard.getColor(turn+1));
    }//GEN-LAST:event_sliderStateChanged

    public void check()
    {
        Thread t2 = new Thread(){
            @Override
            public void run()
            {
                if(lastBid==0 && folded.size()==no_players)
                {
                    System.out.println("Auction.check() if");
                    currentBoard.setEnabled(true);
                    lastRecord.setText("Auction cancelled");
                    lastRecord.setBackground(currentBoard.getColor(turn+1));
                    lastRecord.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                    try {
                        sleep((long)300);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    a.dispose();
                }
                else if(lastBid>0 && folded.size()==(no_players-1))
                {
                    System.out.println("Auction.check() else if");
                    lastRecord.setText("Player " + lastBidder + " won");
                    lastRecord.setBackground(currentBoard.getColor(lastBidder));
                    lastRecord.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                    try {
                        sleep((long)300);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    currentBoard.setEnabled(true);
                    currentBoard.setOwner(lastBidder, position);
                    currentBoard.Transaction(lastBidder, -lastBid);
                    a.dispose();
                }
            }
        };
        t2.start();
    }
    
    public int getBalance(int turn)
    {
        try {
            int balance = 0;
            psql = d.players.prepareStatement("SELECT * FROM players WHERE number=?");
            psql.setInt(1, turn);
            ResultSet players = psql.executeQuery();
            while(players.next())
            {
                balance = players.getInt("balance");
            }
            return balance;
            
        } catch (SQLException ex) {
            Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return balance;
    }
    
    public String getCity(int index)
    {
        String c = "";
        try {
            ResultSet city;
            psql = d.cards.prepareStatement("SELECT * FROM cards WHERE index=?");
            psql.setInt(1, index);
            city = psql.executeQuery();
            while(city.next())
            {
                c = city.getString("name");
            }
            System.out.println("Trade.getCity(): " + c);
            if(c.equals(""))
            {
                psql = d.cards.prepareStatement("SELECT * FROM services WHERE index=?");
                psql.setInt(1, index);
                city = psql.executeQuery();
                while(city.next())
                {
                    c = city.getString("name");
                }
            }
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bid;
    private javax.swing.JLabel card;
    private javax.swing.JLabel currentBid;
    private javax.swing.JLabel currentBidder;
    private javax.swing.JButton fold;
    private javax.swing.JLabel lastRecord;
    private javax.swing.JSlider slider;
    // End of variables declaration//GEN-END:variables
}
