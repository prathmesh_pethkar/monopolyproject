
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Trade extends javax.swing.JFrame {

    DB d;
    PreparedStatement psql;
    Board currentBoard;
    int turn,selectPlayer;
    int no_players;
    int ind1,ind2;
    List<Integer> index1,index2;
    public Trade(Board currentBoard,int turn,int no_players) {
        try {
            d = new DB();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.currentBoard = currentBoard;
        this.turn = turn;
        this.no_players = no_players;
        ind1 = 0;
        ind2 = 0;
        selectPlayer = (turn+1)%no_players;
        index1 = new ArrayList<Integer>();
        index2 = new ArrayList<Integer>();
        index1.add(0);
        index2.add(0);
        initComponents();
        accept.setVisible(false);
        decline.setVisible(false);
        askDiscount.setEnabled(false);
        giveDiscount.setEnabled(false);
        thisPlayer.setText("Player " + (turn+1));
        player.setText("Player " + (selectPlayer+1));
        prevCard.setEnabled(false);
        prevC.setEnabled(false);
        if(no_players==2)
        {
            nextPlayer.setEnabled(false);
            prevPlayer.setEnabled(false);
        }
        
        initialData(turn+1, index1);
        if(index1.size()==1)
        {
            nextC.setEnabled(false);
        }
        offerMoney.setMaximum(getBalance(turn+1));
        initialData(selectPlayer+1, index2);
        if(index2.size()==1)
        {
            nextCard.setEnabled(false);
        }
        askMoney.setMaximum(getBalance(selectPlayer+1));
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        decline = new javax.swing.JButton();
        offeredDiscount = new javax.swing.JLabel();
        nextPlayer = new javax.swing.JButton();
        prevPlayer = new javax.swing.JButton();
        prevCard = new javax.swing.JButton();
        nextCard = new javax.swing.JButton();
        card = new javax.swing.JLabel();
        CardImg = new javax.swing.JLabel();
        askMoneyL = new javax.swing.JLabel();
        giveDiscL = new javax.swing.JLabel();
        cancel = new javax.swing.JButton();
        askMoney = new javax.swing.JSlider();
        giveDiscount = new javax.swing.JSlider();
        accept = new javax.swing.JButton();
        askedDiscount = new javax.swing.JLabel();
        thisPlayer = new javax.swing.JLabel();
        prevC = new javax.swing.JButton();
        ownCard = new javax.swing.JLabel();
        nextC = new javax.swing.JButton();
        ownCardImg = new javax.swing.JLabel();
        offerMoneyL = new javax.swing.JLabel();
        askDiscL = new javax.swing.JLabel();
        offer = new javax.swing.JButton();
        offerMoney = new javax.swing.JSlider();
        askDiscount = new javax.swing.JSlider();
        offeredMoney = new javax.swing.JLabel();
        askedMoney = new javax.swing.JLabel();
        player = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 600));
        setResizable(false);
        setSize(new java.awt.Dimension(400, 600));
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        decline.setBackground(new java.awt.Color(255, 0, 0));
        decline.setText("Decline");
        decline.setEnabled(false);
        decline.setMaximumSize(new java.awt.Dimension(70, 30));
        decline.setMinimumSize(new java.awt.Dimension(70, 30));
        decline.setPreferredSize(new java.awt.Dimension(70, 30));
        decline.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                declineActionPerformed(evt);
            }
        });

        offeredDiscount.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        offeredDiscount.setMaximumSize(new java.awt.Dimension(175, 30));
        offeredDiscount.setMinimumSize(new java.awt.Dimension(175, 30));
        offeredDiscount.setOpaque(true);
        offeredDiscount.setPreferredSize(new java.awt.Dimension(175, 30));

        nextPlayer.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/arrowR.jpg")); // NOI18N
        nextPlayer.setMaximumSize(new java.awt.Dimension(30, 30));
        nextPlayer.setMinimumSize(new java.awt.Dimension(30, 30));
        nextPlayer.setPreferredSize(new java.awt.Dimension(30, 30));
        nextPlayer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextPlayerActionPerformed(evt);
            }
        });

        prevPlayer.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/arrowL.jpg")); // NOI18N
        prevPlayer.setMaximumSize(new java.awt.Dimension(30, 30));
        prevPlayer.setMinimumSize(new java.awt.Dimension(30, 30));
        prevPlayer.setPreferredSize(new java.awt.Dimension(30, 30));
        prevPlayer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevPlayerActionPerformed(evt);
            }
        });

        prevCard.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/arrowL.jpg")); // NOI18N
        prevCard.setMaximumSize(new java.awt.Dimension(30, 30));
        prevCard.setMinimumSize(new java.awt.Dimension(30, 30));
        prevCard.setPreferredSize(new java.awt.Dimension(30, 30));
        prevCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevCardActionPerformed(evt);
            }
        });

        nextCard.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/arrowR.jpg")); // NOI18N
        nextCard.setMaximumSize(new java.awt.Dimension(30, 30));
        nextCard.setMinimumSize(new java.awt.Dimension(30, 30));
        nextCard.setPreferredSize(new java.awt.Dimension(30, 30));
        nextCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextCardActionPerformed(evt);
            }
        });

        card.setMaximumSize(new java.awt.Dimension(70, 30));
        card.setMinimumSize(new java.awt.Dimension(70, 30));
        card.setPreferredSize(new java.awt.Dimension(70, 30));

        CardImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        CardImg.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        CardImg.setMaximumSize(new java.awt.Dimension(175, 185));
        CardImg.setMinimumSize(new java.awt.Dimension(175, 185));
        CardImg.setPreferredSize(new java.awt.Dimension(175, 185));

        askMoneyL.setText("Ask Money");

        giveDiscL.setText("Offer discount on rent:");

        cancel.setBackground(new java.awt.Color(255, 0, 0));
        cancel.setText("Cancel");
        cancel.setMaximumSize(new java.awt.Dimension(70, 30));
        cancel.setMinimumSize(new java.awt.Dimension(70, 30));
        cancel.setPreferredSize(new java.awt.Dimension(70, 30));
        cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelActionPerformed(evt);
            }
        });

        askMoney.setPaintLabels(true);
        askMoney.setPaintTicks(true);
        askMoney.setValue(0);
        askMoney.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                askMoneyStateChanged(evt);
            }
        });

        giveDiscount.setMajorTickSpacing(10);
        giveDiscount.setPaintTicks(true);
        giveDiscount.setValue(0);
        giveDiscount.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                giveDiscountStateChanged(evt);
            }
        });

        accept.setBackground(new java.awt.Color(0, 255, 0));
        accept.setText("Accept");
        accept.setEnabled(false);
        accept.setMaximumSize(new java.awt.Dimension(70, 30));
        accept.setMinimumSize(new java.awt.Dimension(70, 30));
        accept.setPreferredSize(new java.awt.Dimension(70, 30));
        accept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                acceptActionPerformed(evt);
            }
        });

        askedDiscount.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        askedDiscount.setMaximumSize(new java.awt.Dimension(175, 30));
        askedDiscount.setMinimumSize(new java.awt.Dimension(175, 30));
        askedDiscount.setOpaque(true);
        askedDiscount.setPreferredSize(new java.awt.Dimension(175, 30));

        thisPlayer.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        thisPlayer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        thisPlayer.setMaximumSize(new java.awt.Dimension(100, 30));
        thisPlayer.setMinimumSize(new java.awt.Dimension(100, 30));

        prevC.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/arrowL.jpg")); // NOI18N
        prevC.setMaximumSize(new java.awt.Dimension(30, 30));
        prevC.setMinimumSize(new java.awt.Dimension(30, 30));
        prevC.setPreferredSize(new java.awt.Dimension(30, 30));
        prevC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevCActionPerformed(evt);
            }
        });

        ownCard.setMaximumSize(new java.awt.Dimension(70, 30));
        ownCard.setMinimumSize(new java.awt.Dimension(70, 30));
        ownCard.setPreferredSize(new java.awt.Dimension(70, 30));

        nextC.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/arrowR.jpg")); // NOI18N
        nextC.setMaximumSize(new java.awt.Dimension(30, 30));
        nextC.setMinimumSize(new java.awt.Dimension(30, 30));
        nextC.setPreferredSize(new java.awt.Dimension(30, 30));
        nextC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextCActionPerformed(evt);
            }
        });

        ownCardImg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ownCardImg.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        ownCardImg.setMaximumSize(new java.awt.Dimension(175, 185));
        ownCardImg.setMinimumSize(new java.awt.Dimension(175, 185));
        ownCardImg.setPreferredSize(new java.awt.Dimension(175, 185));

        offerMoneyL.setText("Offer Money:");

        askDiscL.setText("Ask for discount on rent:");
        askDiscL.setPreferredSize(new java.awt.Dimension(80, 18));

        offer.setBackground(new java.awt.Color(0, 255, 0));
        offer.setText("Offer");
        offer.setMaximumSize(new java.awt.Dimension(70, 30));
        offer.setMinimumSize(new java.awt.Dimension(70, 30));
        offer.setPreferredSize(new java.awt.Dimension(70, 30));
        offer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                offerActionPerformed(evt);
            }
        });

        offerMoney.setValue(0);
        offerMoney.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                offerMoneyStateChanged(evt);
            }
        });

        askDiscount.setMajorTickSpacing(10);
        askDiscount.setPaintTicks(true);
        askDiscount.setValue(0);
        askDiscount.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                askDiscountStateChanged(evt);
            }
        });

        player.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(askedDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(offeredDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(askDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(giveDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(thisPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ownCardImg, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(prevC, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ownCard, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(nextC, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(offerMoneyL, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(offerMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(askMoneyL, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(prevCard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(card, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(nextCard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addGap(6, 6, 6)
                                    .addComponent(prevPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(player, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(nextPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(CardImg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(askedMoney, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(askMoney, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(accept, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(offer, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(decline, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(askDiscL, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(giveDiscL, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(offeredMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nextPlayer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(prevPlayer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(thisPlayer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(player, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nextC, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prevC, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(prevCard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ownCard, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(card, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nextCard, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ownCardImg, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CardImg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(offerMoneyL)
                    .addComponent(askMoneyL))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(askMoney, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(offerMoney, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(offeredMoney, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(askedMoney, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(giveDiscL)
                    .addComponent(askDiscL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(askDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(giveDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(offeredDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(askedDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(accept, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(offer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cancel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(decline, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 400, 580);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nextPlayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextPlayerActionPerformed
        // TODO add your handling code here:
        ind2 = 0;        
        do
        {
            selectPlayer = (selectPlayer+1)%no_players;
        }while(selectPlayer==turn);
        player.setText("Player " + (selectPlayer+1));
        index2.clear();
        index2.add(0);
        initialData(selectPlayer+1, index2);
        askMoney.setMaximum(getBalance(selectPlayer+1));
        if(index2.size()==1)
        {
            nextCard.setEnabled(false);
        }
        else
        {
            nextCard.setEnabled(true);
        }
        card.setText("");
        prevCard.setEnabled(false);
        prevC.setEnabled(false);
        askDiscount.setEnabled(false);
        giveDiscount.setEnabled(false);
        //CardImg.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/nocard.png"));
    }//GEN-LAST:event_nextPlayerActionPerformed

    private void nextCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextCardActionPerformed
        // TODO add your handling code here:
        if(!prevCard.isEnabled())
        {
            prevCard.setEnabled(true);
           giveDiscount.setEnabled(true);
        }
         
        String city;
        ind2++;
        city = getCity(index2.get(ind2));
        card.setText(city);
        //CardImg.setText("");
        city = city.toUpperCase();
        CardImg.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/Card_"+city+".jpg"));
        if(ind2==(index2.size()-1))
        {
            nextCard.setEnabled(false);
            prevCard.setEnabled(true);
        }
    }//GEN-LAST:event_nextCardActionPerformed

    private void nextCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextCActionPerformed
        // TODO add your handling code here:
        if(!prevC.isEnabled())
        {
            prevC.setEnabled(true);
            askDiscount.setEnabled(true);
        }
        String city;
        ind1++;
        city = getCity(index1.get(ind1));
        ownCard.setText(city);
        //CardImg.setText("");
        city = city.toUpperCase();
        ownCardImg.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/Card_"+city+".jpg"));
        if(ind1==(index1.size()-1))
        {
            nextC.setEnabled(false);
            prevC.setEnabled(true);
        }
    }//GEN-LAST:event_nextCActionPerformed

    private void acceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_acceptActionPerformed
        // TODO add your handling code here:
        currentBoard.setEnabled(true);
        if(ind1==0 && ind2==0)
        {
            if(askMoney.getValue()!=0)
            {
                currentBoard.Transaction(turn+1, askMoney.getValue());
                currentBoard.Transaction(selectPlayer+1, -askMoney.getValue());
            }
            else
            {
                currentBoard.Transaction(turn+1, -offerMoney.getValue());
                currentBoard.Transaction(selectPlayer+1, offerMoney.getValue());
            }
        }
        if(ind1!=0 && ind2==0)
        {
            currentBoard.Transaction(turn+1, askMoney.getValue());
            currentBoard.Transaction(selectPlayer+1, -askMoney.getValue());
            float discount = askDiscount.getValue();
            cardTransfer(turn+1, selectPlayer+1, index1.get(ind1), discount);
            try {
                currentBoard.setColor(index1.get(ind1), currentBoard.getColor(selectPlayer+1), selectPlayer+1);
            } catch (SQLException ex) {
                Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(ind1==0 && ind2!=0)
        {
            currentBoard.Transaction(turn+1, -offerMoney.getValue());
            currentBoard.Transaction(selectPlayer+1, offerMoney.getValue());
            float discount = giveDiscount.getValue();
            cardTransfer(selectPlayer+1, turn+1, index2.get(ind2), discount);
            try {
                currentBoard.setColor(index2.get(ind2), currentBoard.getColor(turn+1), turn+1);
            } catch (SQLException ex) {
                Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(ind1!=0 && ind2!=0)
        {
            if(askMoney.getValue()!=0)
            {
                currentBoard.Transaction(turn+1, askMoney.getValue());
                currentBoard.Transaction(selectPlayer+1, -askMoney.getValue());
            }
            else
            {
                currentBoard.Transaction(turn+1, -offerMoney.getValue());
                currentBoard.Transaction(selectPlayer+1, offerMoney.getValue());
            }
            float discount = giveDiscount.getValue();
            cardTransfer(selectPlayer+1, turn+1, index2.get(ind2), discount);
            discount = askDiscount.getValue();
            cardTransfer(turn+1, selectPlayer+1, index1.get(ind1), discount);
            try {
                currentBoard.setColor(index2.get(ind2), currentBoard.getColor(turn+1), turn+1);
                currentBoard.setColor(index1.get(ind1), currentBoard.getColor(selectPlayer+1), selectPlayer+1);
            } catch (SQLException ex) {
                Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        this.dispose();
    }//GEN-LAST:event_acceptActionPerformed

    private void prevPlayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevPlayerActionPerformed
        // TODO add your handling code here:
        ind2 = 0;
        do
        {
            selectPlayer = (selectPlayer+no_players-1)%no_players;
        }while(selectPlayer==turn);
        player.setText("Player " + (selectPlayer+1));
        index2.clear();
        index2.add(0);
        initialData(selectPlayer+1, index2);
        if(index2.size()==1)
        {
            nextCard.setEnabled(false);
        }
        else
        {
            nextCard.setEnabled(true);
        }
        card.setText("");
        prevCard.setEnabled(false);
        prevC.setEnabled(false);
        askDiscount.setEnabled(false);
        giveDiscount.setEnabled(false);
        //CardImg.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/nocard.png"));
    }//GEN-LAST:event_prevPlayerActionPerformed

    private void prevCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevCardActionPerformed
        // TODO add your handling code here:
        if(!nextCard.isEnabled())
        {
            nextCard.setEnabled(true);
        }
        String city;
        ind2--;
        if(ind2==0)
        {
            prevCard.setEnabled(false);
            nextCard.setEnabled(true);
            card.setText("");
            CardImg.setIcon(null);
            //CardImg.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/nocard.png"));
            giveDiscount.setEnabled(false);
            return;
        }
        city = getCity(index2.get(ind2));
        card.setText(city);
        //CardImg.setText("");
        city = city.toUpperCase();
        CardImg.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/Card_"+city+".jpg"));
    }//GEN-LAST:event_prevCardActionPerformed

    private void prevCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevCActionPerformed
        // TODO add your handling code here:
        if(!nextC.isEnabled())
        {
            nextC.setEnabled(true);
        }
        String city;
        ind1--;
        if(ind1==0)
        {
            prevC.setEnabled(false);
            nextC.setEnabled(true);
            ownCard.setText("");
            ownCardImg.setIcon(null);
            //ownCardImg.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/nocard.png"));
            askDiscount.setEnabled(false);
            return;
        }
        city = getCity(index1.get(ind1));
        ownCard.setText(city);
        //CardImg.setText("");
        city = city.toUpperCase();
        ownCardImg.setIcon(new javax.swing.ImageIcon("/home/prathmesh/Prathmesh/Java Course/Project/Icons/Card_"+city+".jpg"));
    }//GEN-LAST:event_prevCActionPerformed

    private void offerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_offerActionPerformed
        // TODO add your handling code here:
        offerMoneyL.setText("Offered money:");
        askDiscL.setText("Asked for discount on rent:");
        askMoneyL.setText("Asked money:");
        giveDiscL.setText("Offered discount on rent:");
        jPanel1.setEnabled(false);
        //jPanel2.setEnabled(false);
        offer.setVisible(false);
        cancel.setVisible(false);
        //jPanel3.setEnabled(true);
        askedDiscount.setVisible(true);
        askedMoney.setVisible(true);
        offeredMoney.setVisible(true);
        offeredDiscount.setVisible(true);
        askMoney.setEnabled(false);
        askDiscount.setEnabled(false);
        offerMoney.setEnabled(false);
        giveDiscount.setEnabled(false);
        nextC.setEnabled(false);
        nextCard.setEnabled(false);
        nextPlayer.setEnabled(false);
        prevC.setEnabled(false);
        prevCard.setEnabled(false);
        prevPlayer.setEnabled(false);
        accept.setEnabled(true);
        accept.setVisible(true);
        decline.setEnabled(true);
        decline.setVisible(true);
    }//GEN-LAST:event_offerActionPerformed

    private void cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelActionPerformed
        // TODO add your handling code here:
        currentBoard.setEnabled(true);
        this.dispose();
    }//GEN-LAST:event_cancelActionPerformed

    private void declineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_declineActionPerformed
        // TODO add your handling code here:
        currentBoard.setEnabled(true);
        this.dispose();
    }//GEN-LAST:event_declineActionPerformed

    private void askMoneyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_askMoneyStateChanged
        // TODO add your handling code here:
        askedMoney.setText(Integer.toString(askMoney.getValue()));
    }//GEN-LAST:event_askMoneyStateChanged

    private void giveDiscountStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_giveDiscountStateChanged
        // TODO add your handling code here:
        offeredDiscount.setText(Integer.toString(giveDiscount.getValue()));
    }//GEN-LAST:event_giveDiscountStateChanged

    private void offerMoneyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_offerMoneyStateChanged
        // TODO add your handling code here:
        offeredMoney.setText(Integer.toString(offerMoney.getValue()));
    }//GEN-LAST:event_offerMoneyStateChanged

    private void askDiscountStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_askDiscountStateChanged
        // TODO add your handling code here:
        askedDiscount.setText(Integer.toString(askDiscount.getValue()));
    }//GEN-LAST:event_askDiscountStateChanged

    public void cardTransfer(int owner1, int owner2, int index,float discount)
    {
        try {
            if(!currentBoard.isService(index))
            {
                
                psql = d.cards.prepareStatement("UPDATE cards SET owner=?, prevowner=?, discount=? WHERE index=?");
                psql.setInt(1, owner2);
                psql.setInt(2, owner1);
                psql.setFloat(3, discount/(float)100);
                psql.setInt(4, index);
                psql.executeUpdate();
            }
            else
            {
                psql = d.services.prepareStatement("UPDATE services SET owner=?, prevowner=?, discount=? WHERE index=?");
                psql.setInt(1, owner2);
                psql.setInt(2, owner1);
                psql.setFloat(3, discount/(float)100);
                psql.setInt(4, index);
                psql.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void initialData(int player,List<Integer> l)
    {
        try {
            ResultSet data;
            psql = d.cards.prepareStatement("SELECT * FROM cards WHERE owner=? and home=?");
            psql.setInt(1, player);
            psql.setInt(2, 0);
            data = psql.executeQuery();
            while(data.next())
            {
                l.add(data.getInt("index"));
            }
            psql = d.services.prepareStatement("SELECT * FROM services WHERE owner=?");
            psql.setInt(1, player);
            data = psql.executeQuery();
            while(data.next())
            {
                l.add(data.getInt("index"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getCity(int index)
    {
        String c = "";
        try {
            ResultSet city;
            psql = d.cards.prepareStatement("SELECT * FROM cards WHERE index=?");
            psql.setInt(1, index);
            city = psql.executeQuery();
            while(city.next())
            {
                c = city.getString("name");
            }
            if(c.equals(""))
            {
                psql = d.cards.prepareStatement("SELECT * FROM services WHERE index=?");
                psql.setInt(1, index);
                city = psql.executeQuery();
                while(city.next())
                {
                    c = city.getString("name");
                }
            }
            return c;
        } catch (SQLException ex) {
            Logger.getLogger(Trade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }
    
    public int getBalance(int turn)
    {
        int balance = 0;
        try {
            psql = d.players.prepareStatement("SELECT * FROM players WHERE number=?");
            psql.setInt(1, turn);
            ResultSet players = psql.executeQuery();
            while(players.next())
            {
                balance = players.getInt("balance");
            }
            return balance;
            
        } catch (SQLException ex) {
            Logger.getLogger(Auction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return balance;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel CardImg;
    private javax.swing.JButton accept;
    private javax.swing.JLabel askDiscL;
    private javax.swing.JSlider askDiscount;
    private javax.swing.JSlider askMoney;
    private javax.swing.JLabel askMoneyL;
    private javax.swing.JLabel askedDiscount;
    private javax.swing.JLabel askedMoney;
    private javax.swing.JButton cancel;
    private javax.swing.JLabel card;
    private javax.swing.JButton decline;
    private javax.swing.JLabel giveDiscL;
    private javax.swing.JSlider giveDiscount;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton nextC;
    private javax.swing.JButton nextCard;
    private javax.swing.JButton nextPlayer;
    private javax.swing.JButton offer;
    private javax.swing.JSlider offerMoney;
    private javax.swing.JLabel offerMoneyL;
    private javax.swing.JLabel offeredDiscount;
    private javax.swing.JLabel offeredMoney;
    private javax.swing.JLabel ownCard;
    private javax.swing.JLabel ownCardImg;
    private javax.swing.JLabel player;
    private javax.swing.JButton prevC;
    private javax.swing.JButton prevCard;
    private javax.swing.JButton prevPlayer;
    private javax.swing.JLabel thisPlayer;
    // End of variables declaration//GEN-END:variables
}
